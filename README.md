### BankApi REST API

This project exposes some REST CRUD service for a BankApi App. It demonstrates the use of Spring boot, use integration and unit tests (Mockito, Testcontainers), Docker, Flyway, Gradle.
An PostgreSQL database has been used to store the users, accounts and transactions data.

    Important : The project requires Gradle 7.x.x and a Java 8 JDK.

__1. Installation__
Clone the github repository

__2. Set up database configuration and Docker container for image: postgres:14.2 for integration tests.__

__3. Launch the REST server__



