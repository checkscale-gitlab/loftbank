package com.bank.services;

import com.bank.models.User;

public interface UserService {
    User createUser(User user);

    User getUser(Long userId);

    void blockUser(Long userId);

    void unblockUser(Long userId);

    User updateUser(User user, Long userId);

    void deleteUser(Long userId);
}