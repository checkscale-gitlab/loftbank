package com.bank.services;

import com.bank.models.Transaction;

import java.util.List;

public interface TransactionService {
    void makeTransaction(Long userId, Long sourceAccountId, Long targetAccountId, Double amountTransferred);

    List<Transaction> getTransactions(Long userId, Long accountId, Integer offset, Integer limit);

    Transaction getTransaction(Long transactionId);

    Transaction updateTransaction(Transaction transaction);

    void deleteTransaction(Long transactionId);
}
