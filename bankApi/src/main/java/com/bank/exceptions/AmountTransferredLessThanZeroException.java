package com.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Amount of money to transfer cannot be less than zero")
public class AmountTransferredLessThanZeroException extends RuntimeException {
    public AmountTransferredLessThanZeroException(String msg) {
        super(msg);
    }
}