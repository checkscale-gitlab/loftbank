package com.bank.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class TransactionDTO {
    private Long id;
    private Long userId;
    private Instant operationTime;
    private Long sourceAccountId;
    private Long targetAccountId;
    private Double amountTransferred;
}