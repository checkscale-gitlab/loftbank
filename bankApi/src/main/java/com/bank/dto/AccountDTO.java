package com.bank.dto;

import com.bank.models.AccountState;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AccountDTO {
    private Long id;
    private Long userId;
    private double balance;
    private AccountState state;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private LocalDateTime dateCreated;
}
