package com.bank.api;

import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Transaction;
import com.bank.models.User;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class BankApi {
    private Long userCount = 0L;
    private Long accountCount = 0L;
    private Long transactionCount = 0L;

    private List<User> users = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    public User createUser(User user) {
        user.setId(userCount);
        userCount++;
        users.add(user);
        return user;
    }

    public User getUser(Long userId) {
        for (User user : users) {
            if (user.getId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

    public void updateUser(Long userId, User user) {
        User existingUser = getUser(userId);
        if (existingUser == null) {
            throw new IllegalArgumentException();
        }
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        existingUser.setBlocked(user.isBlocked());
    }

    public void deleteUser(Long userId) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).equals(users.get(Math.toIntExact(userId)))) {
                users.remove(i);
                break;
            } else {
                userCount++;
            }
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public Account createAccount(Long userId, Double initialBalance) {
        Account account = new Account();
        account.setId(accountCount);
        accountCount++;
        account.setUserId(userId);
        account.setBalance(initialBalance);
        account.setState(AccountState.ACTIVE);
        accounts.add(account);
        return account;
    }

    public Account getAccount(Long userId, Long accountId) {
        for (Account account : accounts) {
            if (account.getId().equals(accountId) && account.getUserId().equals(userId)) {
                return account;
            }
        }
        return null;
    }

    public List<Account> getAccounts(Long userId) {
        List<Account> accountsByUser = new ArrayList<>();
        for (Account account : accounts) {
            if (account.getUserId().equals(userId)) {
                accountsByUser.add(account);
            }
        }
        return accountsByUser;
    }

    public void blockAccount(Long userId, Long accountId) {
        Account account = getAccount(userId, accountId);
        switch (account.getState()) {
            case ACTIVE:
                account.setState(AccountState.BLOCKED);
            case CLOSED:
                account.setState(AccountState.BLOCKED);
            case BLOCKED:
                throw new IllegalStateException("Account is already BLOCKED.");
        }
    }

    public void unblockAccount(Long userId, Long accountId) {
        Account account = getAccount(userId, accountId);
        switch (account.getState()) {
            case BLOCKED:
                account.setState(AccountState.ACTIVE);
            case CLOSED:
                throw new IllegalStateException("Account is already CLOSED");
            case ACTIVE:
                throw new IllegalStateException("Account is already ACTIVE.");
        }
    }

    public void closeAccount(Long userId, Long accountId) {
        Account account = getAccount(userId, accountId);
        switch (account.getState()) {
            case ACTIVE:
                account.setState(AccountState.CLOSED);
            case CLOSED:
                throw new IllegalStateException("Account is already CLOSED");
            case BLOCKED:
                throw new IllegalStateException("Account is already BLOCKED.");
        }
    }

    public void makeTransaction(Long sourceAccountId, Long targetAccountId, Double amount) {
        Account sourceAccount = null;
        Account targetAccount = null;
        for (Account account : accounts) {
            if (account.getId().equals(sourceAccountId)) {
                sourceAccount = account;
            }
            if (account.getId().equals(targetAccountId)) {
                targetAccount = account;
            }
        }

        if (sourceAccount == null || targetAccount == null) {
            throw new NullPointerException();
        }

        if (sourceAccount.getBalance() >= amount && sourceAccount.getState().equals(AccountState.ACTIVE) && targetAccount.getState().equals(AccountState.ACTIVE)) {
            sourceAccount.setBalance(sourceAccount.getBalance() - amount);
            targetAccount.setBalance(targetAccount.getBalance() + amount);

            Transaction transaction = new Transaction();
            transaction.setId(transactionCount);
            transactionCount++;
            transaction.setOperationTime(Instant.now());
            transaction.setSourceAccountId(sourceAccountId);
            transaction.setTargetAccountId(targetAccountId);
            transaction.setAmountTransferred(amount);
            transactions.add(transaction);

        } else {
            throw new IllegalStateException();
        }
    }

// limit - maximum number of returned transactions
// offset - which element to start with
// for instance, limit = 15, offset = 0, show the first page of 15 elements
// limit = 15, offset = 15, show the second page of 15 elements
// limit = 15, offset = 30, show the third page of 15 elements

    public List<Transaction> getTransactions(Long userId, Long accountId, Integer offset, Integer limit) {
        List<Transaction> transactionsByUser = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if ((transaction.getSourceAccountId().equals(accountId) || transaction.getTargetAccountId().equals(accountId))) {
                transactionsByUser.add(transaction);
            }
        }
        if (offset >= 0 && limit >= 1) {
            List<Transaction> transactionList = transactionsByUser.subList(offset, offset + limit);
            return transactionList;
        } else throw new IllegalStateException();
    }
}