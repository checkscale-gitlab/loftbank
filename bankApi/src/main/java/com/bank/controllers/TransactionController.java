package com.bank.controllers;

import com.bank.dto.TransactionDTO;
import com.bank.models.Transaction;
import com.bank.services.TransactionService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/transactions")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;
    private final ModelMapper modelMapper;

    @PostMapping
    public void makeTransaction(@RequestBody TransactionDTO transactionDTO) {
        transactionService.makeTransaction(transactionDTO.getUserId(), transactionDTO.getSourceAccountId(), transactionDTO.getTargetAccountId(), transactionDTO.getAmountTransferred());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionDTO> getTransaction(@PathVariable("id") Long transactionId) {
        Transaction transaction = transactionService.getTransaction(transactionId);
        // convert transaction to DTO
        TransactionDTO transactionResponse = modelMapper.map(transaction, TransactionDTO.class);
        return ResponseEntity.ok().body(transactionResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TransactionDTO> updateTransaction(@RequestBody TransactionDTO transactionDTO) {
        // convert DTO to transaction
        Transaction transactionRequest = modelMapper.map(transactionDTO, Transaction.class);
        Transaction transaction = transactionService.updateTransaction(transactionRequest);
        // transaction to DTO
        TransactionDTO transactionResponse = modelMapper.map(transaction, TransactionDTO.class);
        return ResponseEntity.ok().body(transactionResponse);
    }

    @DeleteMapping("/{id}")
    public void deleteTransaction(@PathVariable("id") Long id) {
        transactionService.deleteTransaction(id);
    }
}