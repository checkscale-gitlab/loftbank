package com.bank.models;

public enum AccountState {
    ACTIVE, BLOCKED, CLOSED
}
