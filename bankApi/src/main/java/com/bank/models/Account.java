package com.bank.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "accounts")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {
    @Id
    @SequenceGenerator(name = "accountsIdSeq", sequenceName = "accounts_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accountsIdSeq")
    private Long id;
    private Long userId;
    private double balance;
    @Enumerated(value = EnumType.STRING)
    private AccountState state = AccountState.ACTIVE;
    @Column(name = "date")
    private LocalDateTime dateCreated;

    public Account(Long id, Long userId, double balance, AccountState state) {
        this.id = id;
        this.userId = userId;
        this.balance = balance;
        this.state = state;
    }
}
