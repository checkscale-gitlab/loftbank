package com.bank.users;

import com.bank.BankApiApplication;
import com.bank.config.ContainersEnvironment;
import com.bank.models.User;
import com.bank.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
@ActiveProfiles("test-containers")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BankApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRepositoryIntegrationTest extends ContainersEnvironment {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testGetUsersExpectEmptyList() {
        List<User> list = userRepository.findAll();
        assertEquals(0, list.size());
    }
}
