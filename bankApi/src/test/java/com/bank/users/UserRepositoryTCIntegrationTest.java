package com.bank.users;

import com.bank.models.Role;
import com.bank.models.User;
import com.bank.repositories.UserRepository;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertSame;

@Testcontainers
@ActiveProfiles("test-containers")
@SpringBootTest
@ContextConfiguration(initializers = {UserRepositoryTCIntegrationTest.Initializer.class})
public class UserRepositoryTCIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:14.2")
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("postgres");

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                    "spring.jpa.hibernate.ddl-auto=create-drop"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    public void testSaveAndReadUsers() {
        User user1 = new User(1L, "alice@mail.ru", "cooper", "Alice", "Cooper", LocalDateTime.now(), Role.USER, false);
        User user2 = new User(2L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, false);
        User user3 = new User(3L, "ville@mail.ru", "valo", "Ville", "Valo", LocalDateTime.now(), Role.ADMIN, false);
        List<User> userList = Arrays.asList(user1, user2, user3);

        userRepository.saveAll(userList);

        List<User> users = userRepository.findAll();

        assertThat(users).isNotNull();
        assertSame(users.size(), 3);
        assertThat(users.get(0).getFirstName()).isEqualTo("Alice");
        assertThat(users.get(1).getLastName()).isEqualTo("Idol");
        assertThat(users.get(2).isBlocked()).isEqualTo(false);
    }
}
