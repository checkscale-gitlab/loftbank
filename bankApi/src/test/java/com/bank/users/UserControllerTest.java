package com.bank.users;

import com.bank.controllers.UserController;
import com.bank.exceptions.UserNotFoundException;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.services.impl.UserServiceImpl;
import com.bank.utils.JSONUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    User Alice = new User(1L, "alice@mail.ru", "cooper", "Alice", "Cooper", Role.USER, false);
    User Billy = new User(2L, "billy@mail.ru", "idol", "Billy", "Idol", Role.USER, true);
    User Ville = new User(3L, "ville@mail.ru", "valo", "Ville", "Valo", Role.ADMIN, false);

    @Test
    public void controllerInitializedCorrectly() {
        Assertions.assertThat(UserController.class).isNotNull();
    }

    @Test
    public void testCreateUser() throws Exception {
        given(userService.createUser(Alice)).willReturn(Alice);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSONUtils.toJSON(Alice))
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("firstName").value(Alice.getFirstName()))
                .andExpect(jsonPath("lastName").value("Cooper"));
    }

    @Test
    public void testGetUserPositive() throws Exception {
        given(userService.getUser(Billy.getId())).willReturn(Billy);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/" + Billy.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("firstName").value("Billy"));
    }

    @Test
    public void testGetUserNegative() throws Exception {
        Mockito.doThrow(new UserNotFoundException(80L)).when(userService).getUser(80L);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/" + 80)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateUserPositive() throws Exception {
        given(userService.createUser(Billy)).willReturn(Billy);
        given(userService.updateUser(Alice, Billy.getId())).willReturn(Alice);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/" + Billy.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSONUtils.toJSON(Alice))
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("firstName").value(Alice.getFirstName()))
                .andExpect(jsonPath("lastName").value("Cooper"));
    }

    @Test
    public void testUpdateUserNegative() throws Exception {
        User user = new User();
        user.setId(80L);

        Mockito.doThrow(new UserNotFoundException(user.getId())).when(userService).updateUser(user, user.getId());
        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/" + user.getId().toString())
                        .content(mapper.writeValueAsString(user))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteUserPositive() throws Exception {
        doNothing().when(userService).deleteUser(Ville.getId());

        mockMvc.perform(delete("/api/users/" + Ville.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUserNegative() throws Exception {
        User user = new User();
        user.setId(80L);

        Mockito.doThrow(new UserNotFoundException(user.getId())).when(userService).deleteUser(user.getId());

        mockMvc.perform(delete("/api/users/" + user.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
