package com.bank.users;

import com.bank.exceptions.UserNotFoundException;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.repositories.UserRepository;
import com.bank.services.impl.UserServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    User Alice = new User(1L, "alice@mail.ru", "cooper", "Alice", "Cooper", LocalDateTime.now(), Role.USER, false);
    User Billy = new User(2L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, true);
    User Ville = new User(3L, "ville@mail.ru", "valo", "Ville", "Valo", LocalDateTime.now(), Role.ADMIN, false);

    @Test
    public void userServiceImplInitializedCorrectly() {
        Assertions.assertThat(UserServiceImpl.class).isNotNull();
    }

    @Test
    public void testCreateUserPositive() {
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(Alice);
        User userCreated = userService.createUser(Alice);
        assertThat(userCreated.getFirstName()).isSameAs(Alice.getFirstName());
        assertThat(userCreated.getEmail()).isSameAs(Alice.getEmail());

        verify(userRepository).save(Alice);
        verify(userRepository, times(1)).save(Alice);
    }

    @Test
    public void testCreateUserNegative() {
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(Billy);
        User userCreated = userService.createUser(Billy);
        assertNotEquals("", userCreated.getEmail(), Ville.getEmail());
        assertNotEquals("", userCreated.getLastName(), Alice.getLastName());
        assertEquals("", userCreated.getEmail(), Billy.getEmail());
    }

    @Test
    public void testGetUserPositive() {
        when(userRepository.findById(Alice.getId())).thenReturn(Optional.of(Alice));
        User expected = userService.getUser(Alice.getId());

        assertThat(expected).isSameAs(Alice);
        verify(userRepository).findById(Alice.getId());
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUserNegative() {
        given(userRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        userService.getUser(80L);
    }

    @Test
    public void testBlockUserPositive() {
        when(userRepository.findById(Alice.getId())).thenReturn(Optional.of(Alice));
        User user = userService.getUser(Alice.getId());
        assertThat(user.isBlocked()).isSameAs(false);
        userService.blockUser(Alice.getId());
        assertThat(user.isBlocked()).isSameAs(true);

        verify(userRepository).save(Alice);
        verify(userRepository, times(1)).save(Alice);
    }

    @Test(expected = UserNotFoundException.class)
    public void testBlockUserNegative() {
        User notExistingUser = userService.getUser(80L);

        verify(userRepository).save(notExistingUser);
        verify(userRepository, times(1)).save(notExistingUser);
    }

    @Test
    public void testUnBlockUserPositive() {
        when(userRepository.findById(Billy.getId())).thenReturn(Optional.of(Billy));
        User user = userService.getUser(Billy.getId());
        assertThat(user.isBlocked()).isSameAs(true);
        userService.unblockUser(Billy.getId());
        assertThat(user.isBlocked()).isSameAs(false);

        verify(userRepository).save(Billy);
        verify(userRepository, times(1)).save(Billy);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnBlockUserNegative() {
        User notExistingUser = userService.getUser(null);

        verify(userRepository).save(notExistingUser);
        verify(userRepository, times(1)).save(notExistingUser);
    }

    @Test
    public void testUpdateUserPositive() {
        given(userRepository.findById(Alice.getId())).willReturn(Optional.of(Alice));
        userService.updateUser(Billy, Alice.getId());

        verify(userRepository).save(Alice);
        verify(userRepository).findById(Alice.getId());
        assertEquals("", Billy.getEmail(), Alice.getEmail());
        assertNotEquals("", Billy.getId(), Alice.getId());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserNegative() {
        given(userRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        userService.updateUser(Billy, Alice.getId());
    }

    @Test
    public void testDeleteUserPositive() {
        when(userRepository.findById(Alice.getId())).thenReturn(Optional.of(Alice));

        userService.deleteUser(Alice.getId());
        verify(userRepository).delete(Alice);
        verify(userRepository, times(1)).delete(Alice);
    }

    @Test(expected = UserNotFoundException.class)
    public void testDeleteUserNegative() {
        given(userRepository.findById(anyLong())).willReturn(Optional.empty());
        userService.deleteUser(80L);
    }
}
