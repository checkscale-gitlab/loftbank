package com.bank.transactions;

import com.bank.BankApiApplication;
import com.bank.config.ContainersEnvironment;
import com.bank.models.Transaction;
import com.bank.repositories.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
@ActiveProfiles("test-containers")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BankApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransactionRepositoryIntegrationTest extends ContainersEnvironment {

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testGetTransactionsExpectEmptyList() {
        List<Transaction> list = transactionRepository.findAll();
        assertEquals(0, list.size());
    }
}
